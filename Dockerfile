FROM docker.io/alpine:3.12

WORKDIR /app

COPY requirements.txt /app

RUN set -ex \
        && apk add --no-cache bash jq dnsmasq nfs-utils \
        python3 py3-pip \
        openrc && \
                  \
        # remove the default config files
        rm -v /etc/idmapd.conf /etc/exports && \
        \
        # http://wiki.linux-nfs.org/wiki/index.php/Nfsv4_configuration
        mkdir -p /var/lib/nfs/rpc_pipefs                                                     && \
        mkdir -p /var/lib/nfs/v4recovery                                                     && \
        echo "rpc_pipefs  /var/lib/nfs/rpc_pipefs  rpc_pipefs  defaults  0  0" >> /etc/fstab && \
        echo "nfsd        /proc/fs/nfsd            nfsd        defaults  0  0" >> /etc/fstab && \
        apk add --no-cache --virtual .build-deps gcc libc-dev linux-headers python3-dev \
        && ( pip install --ignore-installed distlib --no-cache-dir -r requirements.txt ) \
        && apk del --no-network .build-deps

EXPOSE 2049

COPY . /app

ENTRYPOINT ["/app/boots/entrypoint"]
