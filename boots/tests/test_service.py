import os
from datetime import datetime
import shutil
import tempfile
from pathlib import Path
import pytest
from boots.service import (
    InvalidUsage,
    _download,
    boots_root,
    create_app,
    split_mac_addr,
    dnsmasq_dhcp_hosts_file,
    dnsmasq_options_file,
    encode_pxelinux_filename,
    stamp_dhcp,
    parse_dhcp_hosts,
    parse_download_request,
    pxelinux_root,
    resolve_kernel_filename,
    resolve_nfs_dir,
    resolve_pxelinux_filename,
    stamp_pxelinux_nfs,
    unpack_archive,
    parse_iso8601_date,
)
import time
import responses
from unittest import mock
from unittest.mock import patch
from urllib.parse import urlparse


# This is a bit piggy, but its done to mimic what should happen in the
# production entrypoint
TEST_KERNEL_PATH = 'http://10.42.0.1:9000/boot/test-ci-kernel'
TEST_INITRD_PATH = 'http://10.42.0.1:9000/boot/test-initrd'
os.environ['KERNEL_PATH'] = TEST_KERNEL_PATH
os.environ['INITRD_PATH'] = TEST_INITRD_PATH


@pytest.fixture
def app():
    """Create and configure a new app instance for each test."""
    tmp_dir = tempfile.mkdtemp()
    # create the app with common test config
    app = create_app(
        {
            "TESTING": True,
            "CONFIG_ROOT": Path(tmp_dir),
        }
    )

    # create the database and load test data
    with app.app_context():
        resolve_kernel_filename("/").mkdir(parents=True)
        resolve_nfs_dir("/").mkdir(parents=True)
        pxelinux_root().mkdir(parents=True)

    yield app

    shutil.rmtree(tmp_dir)


@pytest.fixture
def client(app):
    """A test client for the app."""
    yield app.test_client()


@pytest.fixture
def fp():
    def wrapper(rel_path):
        return os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                rel_path
            )
        )
    return wrapper


TEST_TARBALL_URL = "http://testhost/example.tar"
TEST_ERROR_URL = "http://testhost/exception"
TEST_MARS_HOST = "http://mars.test"
TEST_MARS_READY_MACHINES_API = \
    TEST_MARS_HOST + '/api/v1/machine/?ready_for_service=true'


@pytest.fixture
def resp(fp):
    rsps = responses.RequestsMock(False)
    rsps.start()

    headers = {
        'content-length': '166',
        'content-type': 'application/octet-stream',
    }

    with open(fp('example.tar'), 'rb') as f:
        rsps.add(
            responses.GET, TEST_TARBALL_URL, stream=True,
            headers=headers,
            body=f.read()
        )
    rsps.add(responses.GET, TEST_ERROR_URL,
             body=Exception("a random thing happened"))

    yield rsps

    rsps.stop()
    rsps.reset()


def test_parse_iso8601_date():
    assert parse_iso8601_date('2021-02-17T17:04:24.579263Z') == \
        datetime(2021, 2, 17, 17, 4, 24, 579263)

    assert parse_iso8601_date('2021-02-17T17:04:24.579263') == \
        datetime(2021, 2, 17, 17, 4, 24, 579263)

    with pytest.raises(ValueError):
        parse_iso8601_date('garbage.579263')


def test_filepath_accessors(app):
    with app.app_context():
        root = boots_root()
        assert resolve_nfs_dir('/filesystem') == \
            root / 'nfs-roots' / 'filesystem'
        assert resolve_nfs_dir('filesystem') == \
            root / 'nfs-roots' / 'filesystem'
        mac_addr_parts = ['00', '01', '02', '03', '04', '05']
        assert resolve_pxelinux_filename(mac_addr_parts) == \
            root / 'tftp' / 'pxelinux.cfg' / '01-00-01-02-03-04-05'
        assert dnsmasq_dhcp_hosts_file() == root / 'hosts.dhcp'
        assert dnsmasq_options_file() == root / 'options.dhcp'
        assert resolve_kernel_filename('/bzImage') == \
            root / 'tftp' / 'bzImage'


def test_split_mac_addr():
    assert split_mac_addr('00:01:02:03:04:05') == \
        ['00', '01', '02', '03', '04', '05']
    assert split_mac_addr('00-01-02-03-04-05') == \
        ['00', '01', '02', '03', '04', '05']
    assert split_mac_addr('000102030405') == \
        ['00', '01', '02', '03', '04', '05']
    with pytest.raises(ValueError):
        # Too long
        split_mac_addr('00:01:02:03:04:05:06')
    with pytest.raises(ValueError):
        # Too short
        split_mac_addr('00:01:02:03:04')
    with pytest.raises(ValueError):
        # Bad separator
        split_mac_addr('00:01:02:03:04?05')
    with pytest.raises(ValueError):
        split_mac_addr('garbage')


def test_stamp_pxelinux_nfs(app, tmp_path):
    args = {
        'pxelinux_filename': tmp_path /
        encode_pxelinux_filename('00:00:00:00:00:00'),
        'nfs_server_ip': '10.42.0.1',
        'kernel_filesystem_path': '/ci-bzImage',
        'root_filesystem_path': '/igalia-amd-gfx8-1',
        'initrd_path': None,
        'kernel_cmdline_extras': 'amdgpu.option=1'
    }
    stamp_pxelinux_nfs(**args)
    p = tmp_path / "01-00-00-00-00-00-00"
    assert p.read_text() == \
        """DEFAULT default
label default
  KERNEL /ci-bzImage
  APPEND root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.42.0.1:/igalia-amd-gfx8-1,vers=3 init=/init amdgpu.option=1
"""  # noqa: E501

    args['pxelinux_filename'] = \
        tmp_path / encode_pxelinux_filename("00-01-00-01-00-00")
    stamp_pxelinux_nfs(**args)
    p = tmp_path / "01-00-01-00-01-00-00"
    assert p.read_text() == \
        """DEFAULT default
label default
  KERNEL /ci-bzImage
  APPEND root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.42.0.1:/igalia-amd-gfx8-1,vers=3 init=/init amdgpu.option=1
"""  # noqa: E501

    args['nfs_server_ip'] = '10.0.0.1'
    args['kernel_filesystem_path'] = '/another/ci-bzImage'
    args['root_filesystem_path'] = '/igalia-amd-random'
    args['kernel_cmdline_extras'] = 'amdgpu.option=0 debug=1'
    stamp_pxelinux_nfs(**args)
    p = tmp_path / "01-00-01-00-01-00-00"
    assert p.read_text() == \
        """DEFAULT default
label default
  KERNEL /another/ci-bzImage
  APPEND root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.0.0.1:/igalia-amd-random,vers=3 init=/init amdgpu.option=0 debug=1
"""  # noqa: E501


@patch('boots.service.reload_dnsmasq')
def test_stamp_dhcp(mock_dnsmasq_reloader, tmp_path):
    p = tmp_path / 'hosts.dhcp'
    args = {
        'hosts_file': p,
        'mac_addr': '00:00:00:00:00:00',
        'ip_addr': '10.42.0.10',
        'hostname': 'test-host',
    }
    stamp_dhcp(**args)
    assert p.read_text().strip() == \
        "00:00:00:00:00:00,10.42.0.10,set:test-host"
    args['mac_addr'] = '00:00:00:00:00:01'
    stamp_dhcp(**args)
    assert p.read_text() == """00:00:00:00:00:00,10.42.0.10,set:test-host
00:00:00:00:00:01,10.42.0.10,set:test-host
"""
    args['mac_addr'] = '00:00:00:00:00:00'
    args['ip_addr'] = '8.8.8.8'
    args['hostname'] = 'igalia-1'
    stamp_dhcp(**args)
    assert p.read_text() == """00:00:00:00:00:00,8.8.8.8,set:igalia-1
00:00:00:00:00:01,10.42.0.10,set:test-host
"""
    args['mac_addr'] = '00:00:00:00:00:02'
    args['ip_addr'] = '8.1.8.1'
    args['hostname'] = 'igalia-2'
    stamp_dhcp(**args)
    assert p.read_text() == """00:00:00:00:00:00,8.8.8.8,set:igalia-1
00:00:00:00:00:01,10.42.0.10,set:test-host
00:00:00:00:00:02,8.1.8.1,set:igalia-2
"""


def test_parse_dhcp_hosts():
    hosts = parse_dhcp_hosts("""
# A comment is fine
10:62:e5:04:39:89,10.42.0.10,set:igalia-amd-gfx8-1


10:62:e5:0e:0a:54,10.42.0.11,set:igalia-amd-gfx8-2
""")
    assert len(hosts) == 2
    assert ('10:62:e5:04:39:89', '10.42.0.10', 'igalia-amd-gfx8-1') in \
        hosts
    assert ('10:62:e5:0e:0a:54', '10.42.0.11', 'igalia-amd-gfx8-2') in \
        hosts
    hosts = parse_dhcp_hosts("")
    assert len(hosts) == 0
    with pytest.raises(InvalidUsage):
        # Deleted first comma
        hosts = parse_dhcp_hosts("""
10:62:e5:04:39:8910.42.0.10,set:igalia-amd-gfx8-1
""")


def test_parse_download_request():
    req = {}
    with pytest.raises(InvalidUsage):
        parse_download_request(req)

    req = {"path": "http://some.site.org/test"}
    with pytest.raises(InvalidUsage):
        parse_download_request(req)


def test_download_kernel(resp, client):
    data = {
        "path": TEST_TARBALL_URL,
        "name": "bzImage"
    }
    # First call returns 202 created
    r = client.post('/kernels/download', json=data)
    resp = r.json
    task_id = resp["task_id"]
    path = urlparse(resp["link"]).path
    assert r.status_code == 202
    resp = _poll_wait_for_finished(client, path)

    # Check that non-existent tasks return an error
    r = client.get(path[:-1])
    resp = r.json
    assert r.status_code == 404
    assert resp["message"] == f"task {task_id[:-1]} is not running"


@patch('boots.service._download')
def test_check_kernel(mock_download, app, client):
    r = client.get('/kernels/bzImage')
    resp = r.json
    assert resp["message"] == "does not exist"
    assert r.status_code // 400 == 1

    with app.app_context():
        resolve_kernel_filename("/bzImage").touch()
        r = client.get('/kernels/bzImage')
        assert r.status_code == 200

    data = {
        "path": TEST_TARBALL_URL,
        "name": "bzImage",
    }
    r = client.post('/kernels/download', json=data)
    resp = r.json
    assert r.status_code // 400 == 1
    assert resp["message"] == "bzImage already exists"

    data["overwrite"] = True
    r = client.post('/kernels/download', json=data)
    assert r.status_code == 202


def test__download(resp, app):
    mock_fn = mock.Mock()
    task = {}
    with app.app_context():
        _download(TEST_TARBALL_URL, "example", boots_root(), task, mock_fn)
    assert mock_fn.call_count == 1
    assert len(mock_fn.call_args) == 2
    assert int(task["content-length"]) == 166
    assert int(task["content-downloaded"]) == 166
    archive_name, output_name = mock_fn.call_args.args
    assert output_name == 'example'


def test__download_connerror(app):
    mock_fn = mock.Mock()
    with app.app_context():
        _download("http://example.invalid/file.tar", "example",
                  boots_root(), {}, mock_fn)
    assert mock_fn.call_count == 0


def test__download_other_exception(resp, app):
    mock_fn = mock.Mock()
    with app.app_context():
        _download(TEST_ERROR_URL, "example", boots_root(), {}, mock_fn)
    assert mock_fn.call_count == 0


def test_unpack_archive(fp):
    tarfile = fp('example.tar')
    with tempfile.TemporaryDirectory() as tmpdirname:
        unpack_archive(tarfile, tmpdirname)
        total_files = 0
        for root, dirs, files in os.walk(tmpdirname):
            total_files += 1
        assert total_files == 4


def test_check_filesystem(resp, app, client):
    r = client.get('/filesystems/example')
    resp = r.json
    assert resp["message"] == "is not a directory"
    assert r.status_code // 400 == 1

    with app.app_context():
        os.makedirs(resolve_nfs_dir('/example'))
        r = client.get('/filesystems/example')
        assert r.status_code == 200

    data = {
        "path": TEST_TARBALL_URL,
        "name": "example",
        "cache-key": "test-key"
    }
    r = client.post('/filesystems/download', json=data)
    resp = r.json
    assert r.status_code // 400 == 1
    assert resp["message"] == "example already exists"


def test_download_filesystem_no_cache_key(resp, app, client):
    data = {
        "path": TEST_TARBALL_URL,
        "name": "example"
    }
    r = client.post('/filesystems/download', json=data)
    # Filesystem downloads must be done with a cache key
    assert r.status_code == 400


def _poll_wait_for_failed(client, path):
    iterations = 1
    while True:
        r = client.get(path)
        resp = r.json
        if resp["state"] == "failed" or iterations == 10:
            break
        assert resp["state"] == "pending"
        time.sleep(0.05)
        iterations += 1

    assert iterations != 10
    assert r.status_code == 200
    assert resp["state"] == "failed"
    return resp


def _poll_wait_for_finished(client, path):
    iterations = 1
    while True:
        r = client.get(path)
        resp = r.json
        if r.status_code == 303 or iterations == 10:
            break
        assert resp["state"] == "pending"
        time.sleep(0.05)
        iterations += 1

    assert iterations != 10
    assert r.status_code == 303
    assert resp["state"] == "finished"
    return resp


def test_download_filesystem(resp, client):
    data = {
        "path": TEST_TARBALL_URL,
        "name": "example",
        "cache-key": "testing-key",
        "overwrite": True
    }

    r = client.post('/filesystems/download', json=data)
    assert r.status_code == 202
    resp = r.json
    task_id = resp["task_id"]
    path = urlparse(resp["link"]).path
    resp = _poll_wait_for_finished(client, path)

    # Do it again and check the cache was hit this time
    r = client.post('/filesystems/download', json=data)
    assert r.status_code == 202
    resp = r.json
    task_id = resp["task_id"]
    path = urlparse(resp["link"]).path
    resp = _poll_wait_for_finished(client, path)

    assert resp["message"] == "already downloaded testing-key"

    # Check that non-existent tasks return an error
    r = client.get(path[:-1])
    resp = r.json
    assert r.status_code == 404
    assert resp["message"] == f"task {task_id[:-1]} is not running"


def test_download_filesystem_invalid_cache_key(resp, client):
    data = {
        "path": TEST_TARBALL_URL,
        "name": "example",
        "cache-key": "example"  # exists in tarball
    }
    r = client.post('/filesystems/download', json=data)
    assert r.status_code == 202
    resp = r.json
    path = urlparse(resp["link"]).path
    resp = _poll_wait_for_failed(client, path)
    assert "already exists in archive" in resp["message"]


def test_invalid_ip_causes_error(app, client):
    data = {
        "kernel_path": "/ci-bzImage",
        "ip": "not-an-ip",
        "hostname": "test-board-1",
        "cmdline": "root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.42.0.1:/nfs/root-filesystem-1,vers=3 init=/init",
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()

    r = client.post('/duts/00:01:02:03:04:05', json=data)
    resp = r.json
    assert resp["message"] == \
        "'not-an-ip' does not appear to be an IPv4 or IPv6 address"
    assert r.status_code // 400 == 1


@patch('boots.service.reload_dnsmasq')
def test_add_dut_b2c(mock_dnsmasq_reloader, app, client):
    data = {
        "kernel_path": "/ci-bzImage",
        "initrd_path": "/initrd",
        "cmdline": "b2c.container=\"docker://registry.freedesktop.org/mupuf/valve-infra/machine_registration:latest check\" b2c.container=\"docker://registry.freedesktop.org/mupuf/valve-infra/machine_registration:latest sgt_hartman\"",  # noqa: E501
        "ip": "10.42.0.10",
        "hostname": "test-board-1",
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()
        resolve_kernel_filename('/initrd').touch()
        r = client.post('/duts/00:01:02:03:04:05', json=data)
        assert r.status_code == 200
        mock_dnsmasq_reloader.assert_called_once()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        assert p.read_text() == \
            """DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD /initrd
  APPEND b2c.container="docker://registry.freedesktop.org/mupuf/valve-infra/machine_registration:latest check" b2c.container="docker://registry.freedesktop.org/mupuf/valve-infra/machine_registration:latest sgt_hartman"
"""  # noqa: E501,W291
        hosts = parse_dhcp_hosts(dnsmasq_dhcp_hosts_file().read_text())
        assert len(hosts) == 1
        assert ('00:01:02:03:04:05', '10.42.0.10', 'test-board-1') in hosts

        del data["initrd_path"]
        r = client.post('/duts/00:01:02:03:04:05', json=data)
        assert r.status_code == 200


@patch('boots.service.reload_dnsmasq')
def test_add_dut_b2c_default_kernel_and_initrd(mock_dnsmasq_reloader, app, client):
    data = {
        "ip": "10.42.0.10",
        "hostname": "test-board-1",
        "cmdline": 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
    }

    with app.app_context():
        r = client.post('/duts/00:01:02:03:04:05', json=data)
        assert r.status_code == 200
        mock_dnsmasq_reloader.assert_called_once()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        assert p.read_text() == \
            f"""DEFAULT default
label default
  KERNEL {TEST_KERNEL_PATH}
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291


@patch('boots.service.reload_dnsmasq')
def test_add_dut_b2c_default_kernel_and_overridden_initrd(mock_dnsmasq_reloader, app, client):
    data = {
        "ip": "10.42.0.10",
        "hostname": "test-board-1",
        "cmdline": 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
        "initrd_path": "/custom-initrd",
    }

    with app.app_context():
        resolve_kernel_filename('/custom-initrd').touch()
        r = client.post('/duts/00:01:02:03:04:05', json=data)
        assert r.status_code == 200
        mock_dnsmasq_reloader.assert_called_once()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        assert p.read_text() == \
            f"""DEFAULT default
label default
  KERNEL {TEST_KERNEL_PATH}
  INITRD /custom-initrd
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291


@patch('boots.service.reload_dnsmasq')
def test_set_dut_network_configuration(mock_dnsmasq_reloader, app, client):
    data = {
        'cmdline': 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
        "ip": "10.42.0.10",
        "hostname": "test-board-1",
    }

    r = client.post('/duts/00:01:02:03:04:05', json=data)
    assert r.status_code == 200

    mock_dnsmasq_reloader.assert_called_once()
    with app.app_context():
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        hosts_file = dnsmasq_dhcp_hosts_file()
        assert p.read_text() == \
            f"""DEFAULT default
label default
  KERNEL {TEST_KERNEL_PATH}
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291
        assert hosts_file.read_text() == \
            """00:01:02:03:04:05,10.42.0.10,set:test-board-1
"""

    data = {
        "ip": "1.2.3.4",
        "hostname": "elephant",
    }

    r = client.post('/duts/00:01:02:03:04:05/network', json=data)
    assert r.status_code == 200
    assert mock_dnsmasq_reloader.call_count == 2

    with app.app_context():
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        hosts_file = dnsmasq_dhcp_hosts_file()

        assert p.read_text() == \
            f"""DEFAULT default
label default
  KERNEL {TEST_KERNEL_PATH}
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291

        assert hosts_file.read_text() == \
            """00:01:02:03:04:05,1.2.3.4,set:elephant
"""


@patch('boots.service.reload_dnsmasq')
def test_set_dut_network_configuration__no_board(mock_dnsmasq_reloader, app, client):
    data = {
        "ip": "1.2.3.4",
        "hostname": "elephant",
    }

    r = client.post('/duts/00:01:02:03:04:05/network', json=data)
    assert r.status_code == 200
    with app.app_context():
        hosts_file = dnsmasq_dhcp_hosts_file()
        assert hosts_file.read_text() == \
            """00:01:02:03:04:05,1.2.3.4,set:elephant
"""
    assert mock_dnsmasq_reloader.call_count == 1


@patch('boots.service.reload_dnsmasq')
def test_delete_dut_boot_configuration(mock_dnsmasq_reloader, app, client):
    r = client.delete('/duts/00:01:02:03:04:05/boot')
    assert r.status_code == 404

    data = {
        'kernel_path': '/ci-bzImage',
        'cmdline': 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")

    r = client.post('/duts/00:01:02:03:04:05/boot', json=data)
    assert r.status_code == 200
    mock_dnsmasq_reloader.assert_not_called()
    assert p.read_text() == \
        f"""DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291

    r = client.delete('/duts/00:01:02:03:04:05/boot')
    assert r.status_code == 200
    assert not p.exists()


@patch('boots.service.reload_dnsmasq')
def test_raw_boot_configuration(mock_dnsmasq_reloader, app, client):
    data = {
        'kernel_path': '/ci-bzImage',
        'cmdline': 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")

    r = client.post('/duts/00:01:02:03:04:05/boot', json=data)
    assert r.status_code == 200
    mock_dnsmasq_reloader.assert_not_called()
    assert p.read_text() == \
        f"""DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291

    data = {
        "ip": "1.2.3.4",
        "hostname": "elephant",
    }

    r = client.post('/duts/00:01:02:03:04:05/network', json=data)
    assert r.status_code == 200
    with app.app_context():
        hosts_file = dnsmasq_dhcp_hosts_file()
        assert hosts_file.read_text() == \
            """00:01:02:03:04:05,1.2.3.4,set:elephant
"""
    mock_dnsmasq_reloader.assert_called_once()


@patch('boots.service.reload_dnsmasq')
def test_raw_boot_configuration_with_initrd(mock_dnsmasq_reloader, app, client):
    data = {
        'kernel_path': '/ci-bzImage',
        'initrd_path': '/ci-overridden-initrd',
        'cmdline': 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()
        resolve_kernel_filename('/ci-overridden-initrd').touch()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")

    r = client.post('/duts/00:01:02:03:04:05/boot', json=data)
    assert r.status_code == 200
    mock_dnsmasq_reloader.assert_not_called()
    assert p.read_text() == \
        """DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD /ci-overridden-initrd
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291

    data = {
        'kernel_path': '/ci-bzImage',
        'initrd_path': '/ci-overridden-initrd',
        'cmdline': 'root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.42.0.1:/nfs/root-filesystem-1,vers=3 init=/init',
    }

    r = client.post('/duts/00:01:02:03:04:05/boot', json=data)
    assert r.status_code == 200
    mock_dnsmasq_reloader.assert_not_called()
    assert p.read_text() == \
        """DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD /ci-overridden-initrd
  APPEND root=/dev/nfs rw rootfstype=nfs ip=dhcp nfsroot=10.42.0.1:/nfs/root-filesystem-1,vers=3 init=/init
"""  # noqa: E501,W291


@patch('boots.service.reload_dnsmasq')
def test_raw_boot_creation(mock_dnsmasq_reloader, app, client):
    data = {
        'ip': '1.2.3.4',
        'hostname': 'elephant',
        'kernel_path': '/ci-bzImage',
        'cmdline': 'b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto',
    }

    with app.app_context():
        resolve_kernel_filename('/ci-bzImage').touch()
        p = resolve_pxelinux_filename("00:01:02:03:04:05")
        hosts_file = dnsmasq_dhcp_hosts_file()

    r = client.post('/duts/00:01:02:03:04:05', json=data)
    assert r.status_code == 200

    assert p.read_text() == \
        f"""DEFAULT default
label default
  KERNEL /ci-bzImage
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing" nomodeset amdgpu.option=1 b2c.ntp=auto
"""  # noqa: E501,W291

    assert hosts_file.read_text() == \
        """00:01:02:03:04:05,1.2.3.4,set:elephant
"""

    mock_dnsmasq_reloader.assert_called_once()

    data = {
        "ip": "1.1.1.1",
        "hostname": "fish",
    }
    r = client.post('/duts/00:01:02:03:04:05/network', json=data)
    assert r.status_code == 200
    with app.app_context():
        hosts_file = dnsmasq_dhcp_hosts_file()
        assert hosts_file.read_text() == \
            """00:01:02:03:04:05,1.1.1.1,set:fish
"""
    assert mock_dnsmasq_reloader.call_count == 2

    data = {
        'cmdline': 'b2c.container="docker://testing-update" nomodeset b2c.ntp=manual',
    }

    r = client.post('/duts/00:01:02:03:04:05/boot', json=data)
    print(r.json)
    assert r.status_code == 200
    assert p.read_text() == \
        f"""DEFAULT default
label default
  KERNEL {TEST_KERNEL_PATH}
  INITRD {TEST_INITRD_PATH}
  APPEND b2c.container="docker://testing-update" nomodeset b2c.ntp=manual
"""  # noqa: E501,W291

    assert mock_dnsmasq_reloader.call_count == 2
