# Network Boot Service

Provide an encapsulated service for machines to network boot.

It composes the following services to provide a simple network booting
interface,

  - DHCP / BOOTP / PXE: Listens for local boot requests and serves the
	right network bootloader
  - TFTP: Delivers a configured kernel and optionally a ramdisk.
  - NFS: Delivers the rootfs (can be optional if the ramdisk contains
	a permanent rootfs).

The service exports a REST interface to control the above, from which
you can:

 * Given a board and its metadata (MAC, desired hostname, desired
   static IP, board tags, etc), generate the necessary DHCP / TFTP /
   NFS / PXE configuration.
 * Change the kernel/ramdisk/rootfs for a boards next boot.
 * Retrieve built kernels / tarballed filesystems and unpack them into
   the boot server.

## TODO

  - This service probably should not be in charge of downloading anything, it's complexity better handled elsewhere
  - Bring an NFS interface back when/if the need arises

## Tutorial

BOOTS encourages you to serve boot artifacts from HTTP if you can. The
bootloader used by BOOTS supports HTTP transfers for you.

If you would like to use a local artifact, it must be in the right
place for BOOTS' TFTP server to find it. This place is
`$BOOTS_ROOT/tftp`. If you wish to reference a file in this location,
such as `$BOOTS_ROOT/tftp/my-kernel`, you should reference it as
`/my-kernel` in the examples below. BOOTS will handle the mapping into
its internal directory for you.

TODO. The above is kinda dumb and unnecessarily confusing, should just
use absolute paths on the host.

Alternatively, you can ask BOOTS to fetch an artifact over HTTPS to
use locally, and it will place the file in the right place. Here is an
example of fetching a tarball, BOOTS will unpack it into a directory
for you,

```
URL='https://packet.freedesktop.org/mesa-lava/mesa/mesa/2021-02-08-subdir-move/amd64/lava-rootfs.tgz'
status_url=$(http --json POST http://localhost:8087/filesystems/download \
   path=$URL \
   name=igalia-amd-gfx8-1 \
   cache-key=2021-02-08-subdir-move \
   | jq -r '.link')
```

You can monitor the status of this filesystems preparation by polling $status_url,

```
while true ; do http $status_url ; done
...
HTTP/1.1 200 OK
Content-Length: 261
Content-Type: application/json

{
	"content-downloaded": 22978560,
	"content-length": "229831066",
	"link": "http://localhost:8087/tasks/21bd4737126b4947a8f8b872002bb66e",
	"message": "downloading a filesystem",
	"state": "pending",
	"task_id": "21bd4737126b4947a8f8b872002bb66e"
}
...
```

This is how to create a new boot configuration for some machine,

```
cat <<EOF | http --json POST http://localhost:8087/duts/10:62:e5:04:39:89
{
	"kernel_path": "http://10.42.0.1:9000/boot/kernel",
	"initrd_path": "http://10.42.0.1:9000/initramfs.linux_amd64.cpio.xz",
	"cmdline": "console=ttyUSB0 b2c.container=\"-it docker://docker.io/alpine\" nomodeset",
	"ip": "10.42.0.10",
	"hostname": "peterpan"
}
EOF
```

Or, to boot a Mesa CI container with local artifacts,

```
cat <<EOF | http --json POST http://localhost:8087/duts/10:62:e5:04:39:89
{
        "initrd_path": "/v0.6-initramfs.linux_amd64.cpio.xz",
        "kernel_path": "/v0.6-ci-kernel",
        "cmdline": "console=ttyUSB0,115200n8 console=tty0 b2c.cache_device=auto b2c.container=\"-it --tls-verify=false docker://10.42.0.1:8002/chturne/mesa/debian/x86_test-vk:2021-03-16-piglit-runner--2021-02-17-gfxreconstruct-master--52dd4a94044449c8481d18dcdc221a3c636366d2\"",
        "ip": "10.42.0.10",
        "hostname": "peterpan"
}
EOF
```

In this case, BOOTS will complain if the kernel or initrd do not
exist.

You can change boot configuration and network configuration independently,

```
cat <<EOF | http --json POST http://localhost:8087/duts/10:62:e5:04:39:89/network
{
	"ip": "192.168.1.10",
	"hostname": "captainhook"
}
EOF
```

and

```
cat <<EOF | http --json POST http://localhost:8087/duts/10:62:e5:04:39:89/boot
{
	"cmdline": "something completely different"
}
EOF
```

In the above case, a default kernel and ramdisk will be pulled from
the environment variables `$KERNEL_PATH` and `$INITRD_PATH`.
TODO. Nasty but handy.


The boot configuration can be deleted, so that the board boots into the default image configured at the system start time (see the entrypoint),

```
http --json DELETE http://localhost:8087/duts/10:62:e5:04:39:89/boot
```


## Interface (OUT-OF-DATE, Incomplete, TODO)

### Add a new root filesystem

```
URL='https://minio-packet.freedesktop.org/mesa-lava/mesa/mesa/2021-02-08-subdir-move/amd64/lava-rootfs.tgz'
http --json POST http://localhost:8087/filesystems/download \
	path=$URL \
	name=igalia-amd-gfx8-1 \
	cache-key=2021-02-08-subdir-move
```

Parameters
  - `path`: The URL of a filesystem to download. The API will
	transparently handle tarballs, and unpack them for you.
  - `name`: The name to refer to this filesystem when adding boards
	via the other APIs.
  - `cache-key`: If a filesystem has already been unpacked and
	contains this key in its root, it will not download it again. For
	automated situations where the filesystem doesn't change much,
	this helps avoid unnecessary downloads without requiring the
	client to handle caching.

Returns
  - 202 Accepted. The "link" field points to a task to poll for
	progress information.
  - 4xx if you supply invalid inputs, check the message body for details

### Check for task status

```
http http://localhost:8087/tasks/32b06738c1f04bd8ac536c3f3156cd8c
HTTP/1.0 202 ACCEPTED
Content-Length: 168
Content-Type: application/json
Date: Mon, 15 Mar 2021 18:00:38 GMT
Server: Werkzeug/1.0.1 Python/3.8.5

{
	"link": "http://localhost:8087/tasks/32b06738c1f04bd8ac536c3f3156cd8c",
	"message": "downloading a kernel",
	"state": "pending",
	"task_id": "32b06738c1f04bd8ac536c3f3156cd8c"
}
...

HTTP/1.0 303 SEE OTHER
Content-Length: 225
Content-Type: application/json
Date: Mon, 15 Mar 2021 18:01:55 GMT
Server: Werkzeug/1.0.1 Python/3.8.5

{
	"content-downloaded": 8873952,
	"content-length": "8873952",
	"link": "http://localhost:8087/tasks/32b06738c1f04bd8ac536c3f3156cd8c",
	"message": "downloading a kernel",
	"state": "finished",
	"task_id": "32b06738c1f04bd8ac536c3f3156cd8c"
}
```

### Add a new kernel

```
URL='https://minio-packet.freedesktop.org/mesa-lava/mesa/mesa/2021-02-08-subdir-move/amd64/bzImage'
http --json POST http://localhost:8087/kernels/download \
	path=$URL \
	name=ci-kernel-subdir
```

Parameters
  - `path`: The URL of a filesystem to download. The API will
	transparently handle tarballs, and unpack them for you.
  - `name`: The name to refer to this filesystem when adding boards
	via the other APIs.

Returns
  - See above


### Add a new ramdisk

See Add a new kernel

## Setting up a development tree

To be up and ready, type the following commands:

	git clone https://gitlab.freedesktop.org/chturne/boots.git
	cd boots

Build the composed service,

	docker buildx build \
		-t registry.freedesktop.org/chturne/boots . \
		--push

	docker run --privileged --network=host \
		--rm -it \
		--entrypoint bash \
		-e NFS_LOG_LEVEL=DEBUG \
		-e BOOTS_SERVICE_PORT=8087 \
		-e BOOTS_HTTP_SERVICE_PORT=8088 \
		-e PRIVATE_INTERFACE=private \
		-e PYTHONPATH=/app \
		-e BOOT2CONTAINER_RELEASE=v0.6 \
		-v /mnt/tmp/boots:/boots \
		-v /mnt/tmp/boots/nfs-roots:/nfs \
		-v /mnt/tmp/boots/nfs-roots:/boots/tftp/nfs \
		-v /lib/modules:/lib/modules \
		-v $(pwd):/app \
		registry.freedesktop.org/chturne/boots

From `bash`, you can run the development instance like this by starting the entrypoint,

	./entrypoint

Note that on distros with AppArmor (like Ubuntu), the NFS filesystems
will fail to mount. If you care about that, you can configure it as
explained in
https://github.com/ehough/docker-nfs-server/blob/develop/doc/feature/apparmor.md

To check the code formatting,

	tox -e pep8

To run the tests,

	tox -e py38-test

To run the tests, and see the coverage (very slow),

	tox -e py38-coverage

To all of the above,

	tox

Useful check for test consistency,

	pytest -v | egrep -v '\d.\d{2}s' 2>&1>ref
	for i in $(seq 1 100); do
		pytest -v | egrep -v '\d.\d{2}s' 2>&1>new
		echo $i
		if ! diff -q ref new; then
			echo "DIFFERENCE"
			cat ref new
			break
		fi
	done
